# Flyway demo

This repo contains a simple flyway setup with one migration step for a
postgres database.

## Installation

You can either get the command line tool:

    wget https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/5.2.4/flyway-commandline-5.2.4-linux-x64.tar.gz
    tar -xzf flyway-commandline-5.2.4-linux-x64.tar.gz

Or the docker container:

    docker pull boxfuse/flyway:5.2.4

## Running

    flyway -configFiles=flyway.conf -locations=filesystem:sql/ migrate
